Windows Phone 8 SDK 中让人兴奋的一个 API 就是 Proximity API（接近API）。这个 API 可以让应用程序通过蓝牙连接、Wi-Fi Direct 和 NFC 来分享数据。下面这个例子非常简单，没有实际用途，但可作为入门。

这个例子中，使用 PeerFinder 来处理连接的等待和接收，以及异步搜索对等连接点，一旦连接建立就可以通过 socket 来读写二进制数据。相当简单！